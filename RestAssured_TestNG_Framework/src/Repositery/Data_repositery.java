package Repositery;

public class Data_repositery {
	public static String headername() {
		String headername = "content-type";
		return headername;
	}

	public static String headervalue() {
		String headervalue = "application/json";
		return headervalue;

	}

	public static String hostname() {
		String hostname = "https://reqres.in/";
		return hostname;

	}

	public static String resource_post_create() {
		String resource = "api/users";
		return resource;

	}

	public static String resource_reg_succ() {
		String resource = "api/register";
		return resource;
	}

	public static String resource_get_user_list() {
		String resource = "api/users?page=2";
		return resource;
	}

	public static String resource_put_update() {
		String resource = "api/users/2";
		return resource;
	}

	public static String resource_patch_update() {
		String resource = "api/users/2";
		return resource;
	}
	public static String resource_delete() {
		String resource ="api/users/2";
		return resource;
	}
	public static String resource_get_single_user() {
		String resource ="api/users/2";
		return resource;
	}

}
