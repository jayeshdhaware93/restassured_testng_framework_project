package DifferentAnnotations_Testcase;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Repositery.Data_repositery;
import common_trigger_methods.Api_trigger;
import common_trigger_methods.Utility;
import io.restassured.response.Response;

public class Annotation_delete {

	String endpoint;
	File dir_name;

	Response response;

	@BeforeTest
	public void prerequisite_setup() {
		endpoint = Data_repositery.hostname() + Data_repositery.resource_delete();
		response = Api_trigger.delete_trigger(Data_repositery.headername(), Data_repositery.headervalue(), endpoint);
		dir_name = Utility.createLogDirectory("delete_API_log");
	}

	@Test(description = "validate the response body parameter of delete API")
	public void validator() {
		int Stauscode = response.getStatusCode();

		System.out.println("status code is :" + Stauscode);

//		validate status code 
		Assert.assertEquals(Stauscode, 204);
	}

	@AfterTest
	public void evidencefilecreator() throws IOException {
		Utility.evidenceFileCreator(dir_name, Utility.testLogName("delete_API"), endpoint, null,
				response.getHeader("Date"), null);

	}

}
