package DifferentAnnotations_Testcase;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Repositery.Data_repositery;
import Repositery.RequestBody;
import common_trigger_methods.Api_trigger;
import common_trigger_methods.Utility;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;


public class Annotation_dataprovider_different_class {
	File dir_name;
	String requestBody;
	String endpoint;
	Response response;

	@BeforeTest
	public void prerequisite_setup() throws IOException {
		dir_name = Utility.createLogDirectory("postApi_create_log");
		endpoint = Data_repositery.hostname() + Data_repositery.resource_post_create();
//		response = Api_trigger.post_trigger(Data_repositery.headername(), Data_repositery.headervalue(), requestBody,
//				endpoint);

	}

	@Test(dataProvider = "requestBody" , dataProviderClass=Repositery.RequestBody.class , description = "Data_Provider_Different_Class_Test")
	public void validator2(String value_innameORemail,String value_injobORpasw ) throws IOException {
		 requestBody =  "{\r\n"
					+ "    \"name\": \""+value_innameORemail+"\",\r\n"
					+ "    \"job\": \""+value_injobORpasw+"\"\r\n"
					+ "}";

		response = Api_trigger.post_trigger(Data_repositery.headername(), Data_repositery.headervalue(), requestBody,
				endpoint);
		
		// extract response body and status code

		System.out.println(response.getBody().asPrettyString());
		int statusCcode = response.getStatusCode();
		System.out.println("status code is :" + statusCcode);

//					 fetch expected parameters

		System.out.println("---------expected parameters------------");
		JsonPath req_jsn = new JsonPath(requestBody);
		String req_name = req_jsn.getString("name");
		System.out.println(req_name);
		String req_job = req_jsn.getString("job");
		System.out.println(req_job);

//				   fetch response body parameters
		System.out.println("------------response body parameters-------------");
		String res_name = response.getBody().jsonPath().getString("name");
		System.out.println(res_name);
		String res_job = response.getBody().jsonPath().getString("job");
		System.out.println(res_job);
		String res_id = response.getBody().jsonPath().getString("id");
		System.out.println(res_id);
		String res_createdat = response.getBody().jsonPath().getString("createdAt");
		System.out.println(res_createdat);
		res_createdat = res_createdat.substring(0, 11);
		System.out.println(res_createdat);

//				    get local time 

		LocalDateTime curranttime = LocalDateTime.now();
		String exp_time = curranttime.toString().substring(0, 11);

//				 validate using testng

		Assert.assertEquals(statusCcode, 201);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdat, exp_time);
		
		Utility.evidenceFileCreator(dir_name, Utility.testLogName("post_create"), endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asPrettyString());
	}

	@AfterTest
	public void evidencefilecreator() throws IOException {
		
	}

}
