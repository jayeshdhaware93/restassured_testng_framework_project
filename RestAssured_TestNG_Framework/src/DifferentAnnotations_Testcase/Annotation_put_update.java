package DifferentAnnotations_Testcase;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Repositery.Data_repositery;
import Repositery.RequestBody;
import common_trigger_methods.Api_trigger;
import common_trigger_methods.Utility;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Annotation_put_update {
	String endpoint;
	String requestBody;
	File dir_name;
	Response response;

	@BeforeTest
	public void prerequisite_setup() throws IOException {
		endpoint = Data_repositery.hostname() + Data_repositery.resource_put_update();
		requestBody = RequestBody.req_put_update();
		dir_name = Utility.createLogDirectory("put_update_API_log");
		response = Api_trigger.put_update(Data_repositery.headername(), Data_repositery.headervalue(), requestBody,
				endpoint);
	}

	@Test(description = "validate the response body parameter of put_update API")
	public void validator() {
		int StatusCcode = response.getStatusCode();
		System.out.println("\n" + "status code is:" + " " + StatusCcode);
		System.out.println("\n" + "-----response body is ------");

		System.out.println(response.asPrettyString());
//	create request body object to fetch request body parameters
		JsonPath req_jsn = new JsonPath(requestBody);
		System.out.println("\n" + "-----request body parameters-------");
		String req_name = req_jsn.getString(RequestBody.key_innameORemail);
		System.out.println("name:" + req_name);
		String req_job = req_jsn.getString(RequestBody.key_injobORpasw);
		System.out.println("job:" + req_job);

//	extract response body parameters
		System.out.println("\n" + "-------response body parameters-------");

		String res_name = response.getBody().jsonPath().getString(RequestBody.key_innameORemail);
		System.out.println("name:" + res_name);
		String res_job = response.getBody().jsonPath().getString(RequestBody.key_injobORpasw);
		System.out.println("job:" + res_job);
		String res_time = response.getBody().jsonPath().getString("updatedAt");
		System.out.println("updated at:" + res_time);
		res_time = res_time.substring(0, 11);
		System.out.println(res_time);

//	get local time
		LocalDateTime curranttime = LocalDateTime.now();
		String exp_time = curranttime.toString().substring(0, 11);

//	validate response body parameters

		Assert.assertEquals(StatusCcode, 200);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_time, exp_time);

	}

	@AfterTest
	public void eviencefilecreator() throws IOException {
		Utility.evidenceFileCreator(dir_name, Utility.testLogName("put_update"), endpoint, requestBody,
				response.getHeader("Date"), response.getBody().asPrettyString());
	}
}
