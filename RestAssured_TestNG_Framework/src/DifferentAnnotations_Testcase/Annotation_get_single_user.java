package DifferentAnnotations_Testcase;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Repositery.Data_repositery;
import common_trigger_methods.Api_trigger;
import common_trigger_methods.Utility;
import io.restassured.response.Response;

public class Annotation_get_single_user {
	String endpoint;
	Response response;
	File dir_name;

	@BeforeTest
	public void prerequisite_seup() {
		endpoint = Data_repositery.hostname() + Data_repositery.resource_get_single_user();
		dir_name = Utility.createLogDirectory("get_single_user_log");
		response = Api_trigger.get_single_user_trigger(Data_repositery.headername(), Data_repositery.headervalue(),
				endpoint);
	}

	@Test(description = "validate the response body parameter of get_single_user API")
	public void validator() {
		System.out.println(response.asPrettyString());
		int stausCode = response.getStatusCode();
		System.out.println("ststus code is:" + stausCode);

//		get response body parameters
		System.out.println("--------respnse body parameters---------");

		String res_id = response.getBody().jsonPath().getString("data.id");
		System.out.println(res_id);

		String res_email = response.getBody().jsonPath().getString("data.email");
		System.out.println(res_email);

		String res_firstName = response.getBody().jsonPath().getString("data.first_name");
		System.out.println(res_firstName);

		String res_lastName = response.getBody().jsonPath().getString("data.last_name");
		System.out.println(res_lastName);

		String res_url = response.getBody().jsonPath().getString("support.url");
		System.out.println(res_url);

		String res_text = response.getBody().jsonPath().getString("support.text");
		System.out.println(res_text);

//		validate status code and response body parameters
		Assert.assertEquals(stausCode, 200);

		Assert.assertEquals(res_id, "2");
		Assert.assertEquals(res_email, "janet.weaver@reqres.in");
		Assert.assertEquals(res_firstName, "Janet");
		Assert.assertEquals(res_lastName, "Weaver");
		Assert.assertEquals(res_url, "https://reqres.in/#support-heading");
		Assert.assertEquals(res_text, "To keep ReqRes free, contributions towards server costs are appreciated!");

	}

	@AfterTest
	public void evidencefilecreator() throws IOException {
		Utility.evidenceFileCreator(dir_name, Utility.testLogName("get_single_user"), endpoint, null,
				response.getHeader("Date"), response.asPrettyString());
	}
}
