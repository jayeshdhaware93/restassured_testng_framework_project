package DifferentAnnotations_Testcase;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Repositery.Data_repositery;
import Repositery.RequestBody;
import common_trigger_methods.Api_trigger;
import common_trigger_methods.Utility;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Annotation_post_create {

	String requestBody;
	String endpoint;
	Response response;
	File dir_name;

	@BeforeTest

	public void prerquisitr_setup() throws IOException {
		System.out.println("prerequsite set up");
		requestBody = RequestBody.req_post_create("post_TC2");
		endpoint = Data_repositery.hostname() + Data_repositery.resource_post_create();
		response = Api_trigger.post_trigger(Data_repositery.headername(), Data_repositery.headervalue(), requestBody,
				endpoint);
		dir_name = Utility.createLogDirectory("postApi_create_log");
	}

	@Test(description = "validate the response body parameter of post_create API")
	public void validator() throws IOException {
		System.out.println("test case run");
		response = Api_trigger.post_trigger(Data_repositery.headername(), Data_repositery.headervalue(), requestBody,
				endpoint);

		int status_code = response.getStatusCode();
		System.out.println("status code is: " + status_code);

//		get expected parameters
		JsonPath req_jsn = new JsonPath(requestBody);
		String req_name = req_jsn.getString("name");
		System.out.println(req_name);
		String req_job = req_jsn.getString("job");

//		get response body parameters
		String res_name = response.getBody().jsonPath().getString("name");
		System.out.println(res_name);
		String res_job = response.getBody().jsonPath().getString("job");
		System.out.println(res_job);
		String res_id = response.getBody().jsonPath().getString("id");
		System.out.println(res_id);
		String res_createdat = response.getBody().jsonPath().getString("createdAt");
		System.out.println(res_createdat);
		res_createdat = res_createdat.substring(0, 11);
		System.out.println(res_createdat);

//		get local date
		LocalDateTime curranttime = LocalDateTime.now();
		String exp_date = curranttime.toString().substring(0, 11);

//		validate response body parameters
		Assert.assertEquals(status_code, 201);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdat, exp_date);

	}

	@AfterTest
	public void evidenceFileCreator() throws IOException {
		System.out.println("after test method is called");
		Utility.evidenceFileCreator(dir_name, Utility.testLogName("post_create"), endpoint, requestBody,
				response.getHeader("Date"), response.asPrettyString());

	}

}
