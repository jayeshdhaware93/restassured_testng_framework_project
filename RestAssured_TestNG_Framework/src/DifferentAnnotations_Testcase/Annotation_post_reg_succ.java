package DifferentAnnotations_Testcase;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Repositery.Data_repositery;
import Repositery.RequestBody;
import common_trigger_methods.Api_trigger;
import common_trigger_methods.Utility;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Annotation_post_reg_succ {
	String requestBody;
	String endpoint;
	Response response;
	File dir_name;

	@BeforeTest
	public void prerequiste_setup() throws IOException {
		System.out.println("pre-requisite setup");
		dir_name = Utility.createLogDirectory("post_reg_succ_log");
		endpoint = Data_repositery.hostname() + Data_repositery.resource_reg_succ();
		requestBody = RequestBody.req_post_reg_succ();
		response = Api_trigger.post_reg_succ_trigger(Data_repositery.headername(), Data_repositery.headervalue(),
				requestBody, endpoint);

	}

	@Test(description = "validate the response body parameter of post_create_reg_succ API")
	public void validator() throws IOException {
		System.out.println("test case run");
		response = Api_trigger.post_reg_succ_trigger(Data_repositery.headername(), Data_repositery.headervalue(),
				requestBody, endpoint);

		System.out.println(response.getBody().asPrettyString());

//			fetch status code and store i to variable
		int status_code = response.getStatusCode();
		System.out.println("status code is :" + status_code);

//				fetch request body parameters 

		System.out.println("------request body parameters--------");
		JsonPath req_jsn = new JsonPath(requestBody);
		String req_email = req_jsn.getString("email");
		System.out.println(req_email);
		String req_pasw = req_jsn.getString("password");
		System.out.println(req_pasw);

//			fetch response body

		System.out.println("-------response body parameters--------");
		String res_id = response.getBody().jsonPath().getString("id");
		System.out.println(res_id);
		String res_token = response.getBody().jsonPath().getString("token");
		System.out.println(res_token);

//			validate response body parameters and status code
		Assert.assertEquals(status_code, 200);
		;
		Assert.assertNotNull(res_id);
		Assert.assertNotNull(res_token);

	}

	@AfterTest
	public void evidenceFilrCreator() throws IOException {
		System.out.println("after test executed");
		Utility.evidenceFileCreator(dir_name, Utility.testLogName("post_reg_succ"), endpoint, requestBody,
				response.getHeader("Date"), response.asPrettyString());

	}

}
