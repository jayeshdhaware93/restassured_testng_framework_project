package TestNG_TestCase;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Repositery.Data_repositery;
import Repositery.RequestBody;
import common_trigger_methods.Api_trigger;
import common_trigger_methods.Utility;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Post_create2 extends Utility {
	
	@Test
	public static void executor() throws IOException {

		File dir_name = Utility.createLogDirectory("postApi2_create_log");
		String endpoint = Data_repositery.hostname() + Data_repositery.resource_post_create();
		String requestBody = "{\r\n" + "    \"name\": \" pekka  \",\r\n" + "    \"job\": \"gamer \"\r\n" + "}";
		int status_code = 0;
		Response response = Api_trigger.post_trigger(Data_repositery.headername(), Data_repositery.headervalue(),
				requestBody, endpoint);
		for (int i = 0; i < 5; i++) {

			status_code = response.getStatusCode();

			if (status_code == 201) {

				Utility.evidenceFileCreator(dir_name, Utility.testLogName("post_create"), endpoint, requestBody,
						response.getHeader("date"), response.getBody().asPrettyString());
				validator(response, requestBody);
				break;
			} else {
				System.out.println("\n" + " valid status code is not found in " + i + "th iteration");
			}
		}
		if (status_code != 201) {
			System.out.println("\n" + "valid status code is not found at all,hence failing the test case");
		}
	}

	public static void validator(Response response, String requestBody) {
		// extract response body and status code

		System.out.println(response.getBody().asPrettyString());
		int statusCcode = response.getStatusCode();
		System.out.println("status code is :" + statusCcode);

//		 fetch expected parameters

		System.out.println("---------expected parameters------------");
		JsonPath req_jsn = new JsonPath(requestBody);
		String req_name = req_jsn.getString("name");
		System.out.println(req_name);
		String req_job = req_jsn.getString("job");
		System.out.println(req_job);

		
//	   fetch response body parameters
		System.out.println("------------response body parameters-------------");
		String res_name = response.getBody().jsonPath().getString("name");
		System.out.println(res_name);
		String res_job = response.getBody().jsonPath().getString("job");
		System.out.println(res_job);
		String res_id =response.getBody().jsonPath().getString("id");
		System.out.println(res_id);
		String res_createdat = response.getBody().jsonPath().getString("createdAt");
		System.out.println(res_createdat);
		res_createdat = res_createdat.substring(0, 11);
		System.out.println(res_createdat);

//	    get local time 

		LocalDateTime curranttime = LocalDateTime.now();
		String exp_time = curranttime.toString().substring(0, 11);

//	 validate using testng

		Assert.assertEquals(statusCcode, 201);
		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdat, exp_time);

	}
}
