endpoint is :
https://reqres.in/api/users

request body is:
{
    "name": "pekka",
    "job": "gamer"
}

response header date is :
Fri, 08 Mar 2024 03:38:47 GMT

response body is :
{
    "name": "pekka",
    "job": "gamer",
    "id": "59",
    "createdAt": "2024-03-08T03:38:47.647Z"
}

