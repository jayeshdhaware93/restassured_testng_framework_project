endpoint is :
https://reqres.in/api/users

request body is:
{
    "name": "leo  ",
    "job": "artist"
}

response header date is :
Fri, 08 Mar 2024 03:38:09 GMT

response body is :
{
    "name": "leo  ",
    "job": "artist",
    "id": "769",
    "createdAt": "2024-03-08T03:38:09.000Z"
}

