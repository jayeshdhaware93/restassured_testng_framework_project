endpoint is :
https://reqres.in/api/users/2

request body is:
{
    "name": "morpheus",
    "job": "zion resident"
}

response header date is :
Fri, 08 Mar 2024 03:38:45 GMT

response body is :
{
    "name": "morpheus",
    "job": "zion resident",
    "updatedAt": "2024-03-08T03:38:45.231Z"
}

