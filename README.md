# RESTAssured_TestNG_Framework_Project

## Overview

This repository contains a TestNG-based automation framework for testing RESTful APIs using Rest Assured. The framework is designed to provide a scalable and maintainable structure for writing API tests.


Created RestAssured TestNG framework using java programming language encapsulating all our requirements:

## Configuration

1.to configure all Rest APIs and : 
     > Execute

     > Extract response = used rest Assured library

     > Parse the response = used jsonpath class which belongs to rest assured library .

     > Validate the response = used testNG library.
     
2.framework is capable of reusability by creating common methods and centralised configuration files.

   a.Here we have created  repositery package which contains a data_repositery class which contains 
     common data like headername,headervalue,hostname and different resources and another class requestbody 
     which contains all requestbodies at centralised location.

   b.Created common trigger method package which has common utility class contains common utility methods
     such as log directory creation ,evidence file creation and wrting data into excel file.
     Repositery of trigger methods for all API requests.

   c.In common trigger method package, ceated extentlistener class which implements 'ITestListener'
     inbuilt java class. TestNg listeners are java classes that listens to events in a test class and
     executes them if event matches the event being listened to.

    ItTestListener events used: 
    >onStart: Invoked before a test starts.
    >onFinish: Invoked after a test finishes.
    >onTestStart: Invoked before each test method is run.
    >onTestSuccess: Invoked after a test method succeeds.
    >onTestFailure: Invoked after a test method fails.
    >onTestSkipped: Invoked after a test method is skipped.
   
   d. seperate testcase package for each type of execution using testng xml .

   e.Implemented serial class runner,parallel class runner,package runner,achieved datatadriven using testng 
     xml.
   
3.Framework is able to do data driven testing:
  a.through excel sheet by using apache-poi libraries.
  b.by using @Parameter annotation and passing data from xml file using parameter tag in it.
  c.by using @DataProvider annotation by passing data from 1.same class 2.different class

4.Annotations used:
   >@Test :used to define the validator method in test class which is used to test the application
   >@Parameter : used for data driven by passing the data from testng xml itself.
   >@BeforeTest :Used to define the prequisite_setup method in test class which executes before each test
     method to configure endpoint,requestbody,reading excel sheet,creating log directry,creating log file.
   >@AfterTest : Used to define the evidenceFileCreator method in test class  which will be executed post
     the test is done.
   >@DataProvider : for data driven testing by passing the data in 1.same class 2.different class

5.Maven dependencies added : 
      1.rest assured
      2.testNG
      3.ApachePOI-a.poi
                  b.poi-ooxml 
                  c.poi-scratchpad 
                  d.poi-excelant
                  e.poi-examples           
      4.Allure-TestNG 
      5.ExtentReports 

6.Generated Allure report and Extent report.











